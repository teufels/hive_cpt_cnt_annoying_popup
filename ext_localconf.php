<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntAnnoyingPopup',
            'Hivecptcntannoyingpopuprender',
            [
                'AnnoyingPopup' => 'render'
            ],
            // non-cacheable actions
            [
                'AnnoyingPopup' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntannoyingpopuprender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_annoying_popup') . 'Resources/Public/Icons/user_plugin_hivecptcntannoyingpopuprender.svg
                        title = LLL:EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_annoying_popup_domain_model_hivecptcntannoyingpopuprender
                        description = LLL:EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_annoying_popup_domain_model_hivecptcntannoyingpopuprender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntannoyingpopup_hivecptcntannoyingpopuprender
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntannoyingpopuprender >
            }'
        );

    }, $_EXTKEY
);