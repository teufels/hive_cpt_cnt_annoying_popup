
plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntannoyingpopup._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-annoying-popup table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-annoying-popup table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-annoying-popup table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hivecptcntannoyingpopup {
        model {
            HIVE\HiveCptCntAnnoyingPopup\Domain\Model\AnnoyingPopup {
                persistence {
                    storagePid = {$plugin.tx_hivecptcntannoyingpopup.model.HIVE\HiveCptCntAnnoyingPopup\Domain\Model\AnnoyingPopup.persistence.storagePid}
                }
            }
        }
    }
}

plugin.tx_hivecptcntannoyingpopup {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntannoyingpopup.view.layoutRootPath}
    }
    features {
        # skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
}

##
## Lib
## Render annoying popup
##
lib.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender = COA
lib.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender {
	10 = USER
	10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = HiveCptCntAnnoyingPopup
		pluginName = Hivecptcntannoyingpopuprender
		vendorName = HIVE
		controller = Render
		action = render
		settings =< plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.settings
		persistence =< plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.persistence
		view =< plugin.tx_hivecptcntannoyingpopup_hivecptcntannoyingpopuprender.view
	}
}

##
## Fallback for TYPO3 <= 8.6.0 without HIVE and Gulp!
##
[userFunc = HIVE\HiveCptCntAnnoyingPopup\Condition\Match\CurrentTypo3VersionOperatorGivenVersion::match("8.6.0", "<=")]
page {
    includeJSFooterlibs {
        HiveCptCntAnnoyingPopup__js = /typo3conf/ext/hive_cpt_cnt_annoying_popup/Resources/Public/Assets/Js/includeJSFooterlibs/index.js
        HiveCptCntAnnoyingPopup__js.async = 1
    }
}
[global]