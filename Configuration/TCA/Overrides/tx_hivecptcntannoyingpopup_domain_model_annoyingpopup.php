<?php

defined('TYPO3_MODE') or die();

$sModel = 'tx_hivecptcntannoyingpopup_domain_model_annoyingpopup';

$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_16x16.png';

$GLOBALS['TCA'][$sModel]['ctrl']['title'] = 'LLL:EXT:hive_cpt_cnt_annoying_popup/Resources/Private/Language/translation_db.xlf:tx_hivecptcntannoyingpopup_domain_model_annoyingpopup';

$GLOBALS['TCA'][$sModel]['columns']['link']['config'] = [
    'type' => 'input',
    'size' => 30,
    'eval' => 'trim',
    'wizards' => [
        '_PADDING' => 2,
        'link' => [
            'type' => 'popup',
            'title' => 'Link',
            'icon' => 'link_popup.gif',
            'module' => [
                'name' => 'wizard_element_browser',
                'urlParameters' => [
                    'mode' => 'wizard'
                ]
            ],
            'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1'
        ],
    ],
];

$GLOBALS['TCA'][$sModel]['columns']['image'] = [
    'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.images',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
        'appearance' => [
            'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
        ],
        // custom configuration for displaying fields in the overlay/reference table
        // to use the imageoverlayPalette instead of the basic overlay Palette
        'overrideChildTca' => [
            'types' => [
                '0' => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.audioOverlayPalette;audioOverlayPalette,
                                --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.videoOverlayPalette;videoOverlayPalette,
                                --palette--;;filePalette'
                ],
                \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                    'showitem' => '
                                --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ]
            ],
        ],
    ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'])
];