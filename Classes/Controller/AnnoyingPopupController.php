<?php
namespace HIVE\HiveCptCntAnnoyingPopup\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_annoying_popup" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * AnnoyingPopupController
 */
class AnnoyingPopupController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * annoyingPopupRepository
     *
     * @var \HIVE\HiveCptCntAnnoyingPopup\Domain\Repository\AnnoyingPopupRepository
     * @inject
     */
    protected $annoyingPopupRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {
        $aHtml = [];
        $sCookie = 'hive-cpt-cnt-annoying-popup--';
        /*
         * Get current page uid
         */

        $iPageUid = $GLOBALS['TSFE']->id;
        /*
         * Find all AnnoyingPopup objects
         *
         * @var \HIVE\HiveCptCntAnnoyingPopup\Domain\Model\AnnoyingPopup[] $oAnnoyingPopups
         */

        $oAnnoyingPopups = $this->annoyingPopupRepository->findAll();
        /*
         * Iterate $aAnnoyingPopups
         */

        if ($oAnnoyingPopups != null) {
            foreach ($oAnnoyingPopups as $oAnnoyingPopup) {
                $sShowOnPages = $oAnnoyingPopup->getShowOnPages();
                $aShowOnPages = explode(',', $sShowOnPages);
                if (in_array($iPageUid, $aShowOnPages)) {
                    if (!isset($_COOKIE[$sCookie . $oAnnoyingPopup->getUid()])) {
                        $aAnnoyingPopup = [
                            $oAnnoyingPopup,
                            $oAnnoyingPopup->getCookieLifetime() > 0 ? gmdate("D, d-M-Y H:i:s e", time() + $oAnnoyingPopup->getCookieLifetime()) : '0'
                        ];
                        $aHtml[] = $aAnnoyingPopup;
                    }
                }
            }
        }
        $this->view->assign('aHtml', $aHtml);
    }
}
